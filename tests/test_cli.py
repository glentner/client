import collections
import json
import unittest
from unittest.mock import patch, MagicMock, mock_open

from click.testing import CliRunner

from antares_client.cli.commands.stream import stream
from antares_client.cli.commands.search import search


class TestSearch(unittest.TestCase):
    @patch("antares_client.cli.commands.search.antares_client.search._raw_search")
    def test_search_can_load_json(self, raw_search):
        raw_search.return_value = b"result"
        result = CliRunner().invoke(search, ["tests/data/search/query.json"])
        self.assertEqual(result.exit_code, 0)
        with open("tests/data/search/query.json") as f:
            test_query = json.load(f)
        self.assertEqual(raw_search.call_args[0][0], test_query)

    @patch("antares_client.cli.commands.search.antares_client.search._raw_search")
    def test_search_exits_with_non_zero_code_if_error(self, raw_search):
        raw_search.side_effect = Exception
        result = CliRunner().invoke(search, ["tests/data/search/query.json"])
        self.assertNotEqual(result.exit_code, 0)


class TestCliMain(unittest.TestCase):
    def setUp(self):
        self.patchers = {
            "mock_json": patch("antares_client.cli.commands.stream.json"),
            "mock_log": patch("antares_client.cli.commands.stream.logger"),
            "MockClient": patch("antares_client.cli.commands.stream.Client"),
            "mock_get_alert_id": patch(
                "antares_client.cli.commands.stream.get_alert_id",
                return_value="test_alert_id",
            ),
        }
        self.mock_alert = {"key": "val"}
        self.mock_get_alert_id = self.patchers["mock_get_alert_id"].start()
        self.mock_json = self.patchers["mock_json"].start()
        self.mock_log = self.patchers["mock_log"].start()
        self.MockClient = self.patchers["MockClient"].start()
        mock_client = MagicMock()
        mock_client.iter.return_value = [("test_topic", self.mock_alert)]
        self.MockClient.return_value.__enter__.return_value = mock_client

    def tearDown(self):
        for patcher in self.patchers.values():
            patcher.stop()

    @patch("antares_client.cli.commands.stream.os.makedirs")
    def test_cli_writes_alert_to_file(self, *_):
        with patch.dict("antares_client.cli.__builtins__", {"open": mock_open()}) as _:
            CliRunner().invoke(
                stream, ["test_topic", "--output-directory", "/some/dir"]
            )
        self.assertEqual(self.mock_json.dump.call_args[0][0], self.mock_alert)

    def test_cli_logs_alert_if_no_output_directory(self, *_):
        CliRunner().invoke(stream, ["test_topic"])
        self.mock_log.info.assert_any_call("Received alert on topic 'test_topic'")
