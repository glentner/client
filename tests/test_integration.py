import json
import os
import unittest
import zlib
from unittest.mock import patch

import bson
import confluent_kafka
from nose.plugins.attrib import attr

import antares_client
import antares_client.cli
from antares_client import Client


def load_test_alerts():
    alerts = []
    alert_directory = os.path.join(os.path.dirname(__file__), "data", "alerts")
    for alert_path in os.listdir(alert_directory):
        with open(os.path.join(alert_directory, alert_path), "r") as f:
            yield json.load(f)


load_test_alerts.__test__ = False  # Hide from nose
TEST_ALERTS = {alert["new_alert"]["alert_id"]: alert for alert in load_test_alerts()}


@attr("requires-kafka")
class TestSinglePartitionKafkaTopicIntegration(unittest.TestCase):
    def setUp(self):
        topic = "single_partition_topic"
        producer = confluent_kafka.Producer({"bootstrap.servers": "kafka:29092"})
        for alert in TEST_ALERTS.values():
            producer.produce(topic, zlib.compress(bson.dumps(alert)))
        producer.flush()
        self.client = Client(
            ["single_partition_topic"],
            **{"host": "kafka", "port": 29092, "group": self.id()}
        )

    def tearDown(self):
        self.client.close()

    def test_client_can_poll_a_message(self):
        alert = self.client.poll(timeout=-1)
        self.assertIsNotNone(alert)

    def test_client_can_poll_all_messages(self):
        alerts = []
        for i in range(len(TEST_ALERTS)):
            topic, alert = self.client.poll()
            alerts.append(alert)
        for alert in alerts:
            alert_id = alert["new_alert"]["alert_id"]
            self.assertDictEqual(alert, TEST_ALERTS[alert_id])

    def test_client_iter_gets_all_messages(self):
        alerts = []
        for topic, alert in self.client.iter(num_alerts=len(TEST_ALERTS)):
            alert_id = alert["new_alert"]["alert_id"]
            self.assertDictEqual(alert, TEST_ALERTS[alert_id])


@attr("requires-kafka")
class TestAutoCommitDisabledKafkaTopicIntegration(unittest.TestCase):
    def setUp(self):
        topic = "single_partition_topic"
        producer = confluent_kafka.Producer({"bootstrap.servers": "kafka:29092"})
        for alert in TEST_ALERTS.values():
            producer.produce(topic, zlib.compress(bson.dumps(alert)))
        producer.flush()
        self.client = Client(
            ["single_partition_topic"],
            **{
                "host": "kafka",
                "port": 29092,
                "group": self.id(),
                "enable_auto_commit": False,
            }
        )

    def tearDown(self):
        self.client.close()

    def test_client_doesnt_commit(self):
        alerts = []
        for i in range(len(TEST_ALERTS)):
            topic, alert = self.client.poll()
            alerts.append(alert)
        for alert in alerts:
            alert_id = alert["new_alert"]["alert_id"]
            self.assertDictEqual(alert, TEST_ALERTS[alert_id])
        self.client.close()
        self.client = Client(
            ["single_partition_topic"],
            **{
                "host": "kafka",
                "port": 29092,
                "group": self.id(),
                "enable_auto_commit": False,
            }
        )
        alerts = []
        for i in range(len(TEST_ALERTS)):
            topic, alert = self.client.poll()
            alerts.append(alert)
        for alert in alerts:
            alert_id = alert["new_alert"]["alert_id"]
            self.assertDictEqual(alert, TEST_ALERTS[alert_id])

    # def test_client_can_manually_commit(self):
    #     alerts = []
    #     for i in range(len(TEST_ALERTS)):
    #         topic, alert = self.client.poll()
    #         self.client.commit()
    #         alerts.append(alert)
    #     for alert in alerts:
    #         alert_id = alert["new_alert"]["alert_id"]
    #         self.assertDictEqual(alert, TEST_ALERTS[alert_id])
    #     self.client.close()
    #     self.client = Client(
    #         ["single_partition_topic"],
    #         **{
    #             "host": "kafka",
    #             "port": 29092,
    #             "group": self.id(),
    #             "enable_auto_commit": False,
    #         }
    #     )
    #     self.assertIs(self.client.poll(timeout=1), (None, None))


@attr("requires-kafka")
class TestMultiplePartitionKafkaTopicIntegration(unittest.TestCase):
    def setUp(self):
        topic = "multiple_partition_topic"
        producer = confluent_kafka.Producer({"bootstrap.servers": "kafka:29092"})
        for alert in TEST_ALERTS.values():
            producer.produce(topic, zlib.compress(bson.dumps(alert)))
        producer.flush()
        self.client = Client(
            ["multiple_partition_topic"],
            **{"host": "kafka", "port": 29092, "group": self.id()}
        )

    def tearDown(self):
        self.client.close()

    def test_client_can_poll_a_message(self):
        alert = self.client.poll(timeout=-1)
        self.assertIsNotNone(alert)

    def test_client_can_poll_all_messages(self):
        alerts = []
        for i in range(len(TEST_ALERTS)):
            topic, alert = self.client.poll()
            alerts.append(alert)
        for alert in alerts:
            alert_id = alert["new_alert"]["alert_id"]
            self.assertDictEqual(alert, TEST_ALERTS[alert_id])

    def test_client_iter_gets_all_messages(self):
        alerts = []
        for topic, alert in self.client.iter(num_alerts=len(TEST_ALERTS)):
            alert_id = alert["new_alert"]["alert_id"]
            self.assertDictEqual(alert, TEST_ALERTS[alert_id])
