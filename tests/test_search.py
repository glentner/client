import json
import unittest
from unittest.mock import patch, MagicMock

from antares_client import search


class TestSearch(unittest.TestCase):
    def setUp(self):
        with open("tests/data/search/query.json") as f:
            self.query = json.load(f)
        with open("tests/data/search/result_set.json") as f:
            self.result_set = json.load(f)

    def test_raw_search_raises_value_error_for_invalid_output_format(self):
        with self.assertRaises(ValueError):
            search._raw_search(self.query, output_format="tsv")

    @patch("antares_client.search._raw_search")
    def test_search_can_load_response_as_json(self, raw_search):
        raw_search.return_value = json.dumps(self.result_set).encode()
        result_set = search.search(self.query)
        self.assertEqual(result_set, self.result_set)
