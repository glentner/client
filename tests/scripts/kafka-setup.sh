#!/bin/bash

kafka-topics \
    --zookeeper=zookeeper:2181 \
    --create \
    --partitions=3 \
    --replication-factor=1 \
    --topic=multiple_partition_topic

kafka-topics \
    --zookeeper=zookeeper:2181 \
    --create \
    --partitions=1 \
    --replication-factor=1 \
    --topic=single_partition_topic
