Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`__

__ https://keepachangelog.com/en/1.0.0/

v0.3.0
------

Added
~~~~~

- The ``search`` subcommand, for searching and downloading querysets from the
  ANTARES ElasticSearch database.

- Started using ``click`` for CLI tooling.

- Initial release of documentation.

Fixed
~~~~~

- Verification of SSL certs in requests to the ANTARES portal for thumbnails.

Changed
~~~~~~~

- Renamed CLI ``antares-client`` to ``antares stream``.

v0.2.2
------

Added
~~~~~

- Support for custom Kafka commit behavior.

v0.2.0
------

Added
~~~~~

- ``antares_client.thumbnails`` module for downloading alert thumbnail images.

v0.1.0
------

Fixed
~~~~~

- \#6: ``_locate_ssl_certs_file`` was called in the ``Client`` constructor even
  if an SSL cert path was provided.

v0.0.1
------

Added
~~~~~

- Initial release
