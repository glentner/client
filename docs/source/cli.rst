.. include:: global.rst.inc

.. _cli:


CLI Usage
=========

Installation
------------

The ANTARES client provides a number of helpful utilities that can be
used from the command line. If you haven't already, follow the instructions
at :ref:`installation`.

Verify that things were installed correctly by running:

.. code:: bash

   antares --version

Usage
-----

The ANTARES command line tool provides functionality through subcommands to the
``antares`` command. Similar to something like ``git``--where you call ``git
pull``, ``git merge``, etc.--here you'll use ``antares search``, ``antares stream``,
etc.

You can pass the ``--help`` option to any subcommand for usage examples and
listings of required arguments and available option.

Reference
---------

The entire CLI suite is documented here but, due to some quirks in our
documentation build system, this section may be a bit difficult to read. I'll
offer the following tips: 1) The reference documentation for each subcommand
shows examples then options then arguments. 2) Navigating to the subcommand
you're interested in is best done through the sidebar or the following links:

* `search <cli.html#antares-search>`_: Search the ANTARES database for particular alerts.
* `stream <cli.html#antares-stream>`_: Stream historical and current alerts from ANTARES.

.. click:: antares_client.cli.cli:entry_point
   :prog: antares
   :show-nested:

