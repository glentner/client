.. _api:

API Reference
=============

.. module:: antares_client

Streaming Client
----------------

.. autoclass:: Client
   :members:
   :inherited-members:
 

Search
------

.. automodule:: antares_client.search
   :members:

Thumbnails
----------

.. automodule:: antares_client.thumbnails
   :members:
