.. include:: global.rst.inc

.. _installation:

Installation and Setup
======================

The ANTARES client supports Python versions 3.4 and up. Python 2.x is not
supported.

Install using ``pip``:

.. code:: bash

   $ pip install antares-client

Verify the client installed correctly:

.. code:: bash

   $ antares --version
   antares, version v0.3.0

Installing for Development
--------------------------

If you plan on contributing to the development of the ANTARES client or if you
want to run the latest (unreleased) version of the library, you may want to
install in development mode.

.. code:: bash

   $ pip install antares-client
   $ git clone https://gitlab.com/noao/antares/client
   $ cd client
   $ python setup.py develop
   $ antares --version
   antares, version v0.3.0

Obtaining Credentials
---------------------

You'll need to contact the ANTARES team to request API credentials if you'd
like to access the real-time alert stream.

These credentials are different than your sign-in credentials for the `ANTARES
Portal`_. We typically grant only one set of credentials per institution. Do
not share these credentials with others. We request that you operate only one
active streaming client per set of credentials, except with permission.

We reserve the right to monitor your usage and revoke your credentials at any
time.
