Version Migration
=================

While the ANTARES client is still in beta (pre-1.0) we will likely make a
handful of breaking changes. This document will help ease your migratory pains.
Thank you for supporting us in our infancy!

.. toctree::
   :maxdepth: 1
   :caption: Contents:

0.2.x to 0.3.x
--------------

- The CLI command for streaming alerts from ANTARES, ``antares-client``, has
  been renamed ``antares stream``. Additionally, in 0.2.0 you were able to pass
  topics to ``antares-client`` in a comma-separated list--this functionality
  has been removed in favor of space-separated lists. So, for example, this
  0.2.0 call:

.. code:: bash

   antares-client extragalactic,nuclear_transient

should be replaced with this 0.3.0 call:

.. code:: bash

   antares stream extragalactic nuclear_transient
