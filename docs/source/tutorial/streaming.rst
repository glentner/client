.. _tutorial-streaming:

Streaming Alerts
================

ANTARES distributes alerts over topic-specific streams--like the ``extragalactic``
or ``nuclear_transient`` streams. The ANTARES client provides a high-level
interface for receiving alerts from these streams with the ``Client`` object.
We will highlight a few of the most useful ways you can use the object here, but have
a look at the :ref:`api` for the full documentation. 

To import and instantiate the ``Client``:

.. code:: python

   from antares_client import Client
   client = Client(
       topics=["extragalactic", "nuclear_transient"],
       api_key="********************",
       api_secret="********************",
   )

The ``poll`` method can be used to retrieve an alert. It returns a
``(topic, alert)`` tuple where ``topic`` is a string (in this example either
``"extragalactic"`` or ``"nuclear_transient"``) and ``alert`` is a dictionary
containing the alert's properties. By default, this method will block
indefinitley, waiting for an alert. If you pass an argument to the ``timeout``
keyword, the method will return ``(None, None)`` after ``timeout`` seconds have
elapsed:

.. code:: python

   topic, alert = client.poll(timeout=10)
   if alert:
       print("recieved an alert")
   else:
       print("waited 10 seconds but didn't get an alert")

Often you'll want to consume alerts in real-time from ANTARES. This pattern is
common enough that ``Client`` objects provide the ``iter`` method:

.. code:: python

  # This...
  for topic, alert in client.iter():
      print("received {} on {}".format(alert, topic))

  # Is equivalent to...
  while True:
      topic, alert = client.poll()
      print("recieved {} on {}".format(alert, topic))

Note, though, that you're unable to control timeout behavior with the former
approach.

You should also close your connection to the client after you're done
which you can do explicitly with the ``close`` method. The ``Client``
also wraps this behavior in a context manager so:

.. code:: python

   # This...
   with Client(topics, **config) as client:
       for topic, alert in client.iter():
           print("recieved {} on {}".format(alert, topic))

   # Is equivalent to...
   client = Client(topics, **config)
   try:
       for topic, alert in client.iter():
           print("recieved {} on {}".format(alert, topic))
   finally:
       client.close()

Example
-------

.. code:: python

   """
   Use this script as a starting point for streaming alerts from ANTARES.

   Author: YOUR_NAME

   """

   from antares_client import Client

   TOPICS = ["extragalactic", "nuclear_transient"]
   CONFIG = {
       api_key: "YOUR_API_KEY",
       api_secret: "YOUR_API_SECRET",
   }


   def process_alert(topic, alert):
       """Put your code here!"""
       pass

   
   def main():
      with Client(TOPICS, **CONFIG) as client:
          for topic, alert in client.iter():
              process_alert(alert, topic)
   
   
   if __name__ == "__main__":
       main()
