Downloading Thumbnails
======================

ANTARES keeps ZTF image thumbnails for all alerts which we store or stream out.
You can retrieve these thumbnails like so:

.. code:: python

   from antares_client.thumbnails import get_thumbnails
   alert_id = 9898524
   thumbnails = get_thumbnails(alert_id)
   print(thumbnails)
   # {
   #     'template': {
   #         'file_name': '...fits.gz',
   #         'file_bytes': '...'
   #     },
   #     'science': {
   #         'file_name': '...fits.gz',
   #         'file_bytes': '...'
   #     },
   #     'difference': {
   #         'file_name': '...fits.gz',
   #         'file_bytes': '...'
   #     }
   # }

You can also write them to disk. The return value is the same as before:

.. code:: python

   alert_id = 9898524
   output_dir = "/foo/bar/thumbnails/"
   thumbnails = get_thumbnails(alert_id, output_dir)
   print(thumbnails)
   # {
   #     'template': {
   #         'file_name': '...fits.gz',
   #         'file_bytes': '...'
   #     },
   #     'science': {
   #         'file_name': '...fits.gz',
   #         'file_bytes': '...'
   #     },
   #     'difference': {
   #         'file_name': '...fits.gz',
   #         'file_bytes': '...'
   #     }
   # }
